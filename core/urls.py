from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
from django.views.generic import TemplateView

from tastypie.api import Api
from reservations.api import ReservationResource
from rooms.api import RoomResource
from users.api import UserResource
from .views import Home

admin.autodiscover()

v1_api = Api(api_name='v1')
v1_api.register(ReservationResource())
v1_api.register(RoomResource())
v1_api.register(UserResource())

urlpatterns = patterns('',
    url(r'^$', Home.as_view(), name='home'),
    url(r'^api/', include(v1_api.urls)),
    url(r'^account/', include('registration.backends.default.urls')),
    url(r'', include('users.urls', namespace='users')),
    url(r'^room/', include('rooms.urls', namespace='rooms')),
    url(r'^reserve/', include('reservations.urls', namespace='reservations')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += staticfiles_urlpatterns()