from django import template
from django.forms import CheckboxInput
import re

register = template.Library()

@register.filter(name='is_checkbox')
def is_checkbox(field):
    print field.field.widget.__class__.__name__
    return field.field.widget.__class__.__name__ == CheckboxInput().__class__.__name__

@register.filter(name='is_textarea')
def is_textarea(field):
    return field.field.widget.__class__.__name__ == 'Textarea'

@register.filter(name='is_select')
def is_select(field):
    return field.field.widget.__class__.__name__ == 'Select'

@register.filter(name='is_datetime')
def is_datetime(field):
    return field.field.widget.__class__.__name__ == 'DateTimeInput'

@register.filter(name='is_password')
def is_password(field):
    return field.field.widget.__class__.__name__ == 'PasswordInput'

@register.filter(name='is_input')
def is_input(field):
    print field.field.widget
    return re.search('Input', field.field.widget.__class__.__name__)

