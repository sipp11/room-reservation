from django.views.generic import TemplateView
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from rooms.models import Room

class MessageMixin(object):
    """
    Make it easy to display notification messages when using Class Based Views.
    """
    message = None
    default_message = _('successfully processed') 

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.message or self.default_message)
        return super(MessageMixin, self).delete(request, *args, **kwargs)

    def form_valid(self, form):
        messages.success(self.request, self.message or self.default_message)
        return super(MessageMixin, self).form_valid(form)


class Home(TemplateView):
    template_name = 'home.html'


    def get_context_data(self, **kwargs):
        context = super(Home, self).get_context_data(**kwargs)
        context['rooms'] = Room.objects.all()
        return context