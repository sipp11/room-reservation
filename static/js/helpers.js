function renderTemplate(templateId, context) {
    var template = Handlebars.compile($(templateId).html());
    return template(context);
}

function getReservationData(tId) {
    var apiUrl = '/api/v1/reservation/'+tId+'/?format=json';
    return $.get(apiUrl);
}

function postReservationData(data) {
    return $.ajax({
        type: "post",
        url: "/api/v1/reservation/",
        contentType: 'application/json',
        data: JSON.stringify(data)
    });
}

function updateReservationData(id, data) {
    var apiUrl = '/api/v1/reservation/'+id+'/';
    return $.ajax({
        type: "patch",
        url: apiUrl,
        contentType: 'application/json',
        data: JSON.stringify(data)
    });
}
