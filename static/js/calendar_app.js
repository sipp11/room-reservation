window.TastypieModel = Backbone.Model.extend({
    base_url: function() {
      var temp_url = Backbone.Model.prototype.url.call(this);
      return (temp_url.charAt(temp_url.length - 1) == '/' ? temp_url : temp_url+'/');
    },

    url: function() {
      return this.base_url();
    }
});

window.TastypieCollection = Backbone.Collection.extend({
    parse: function(response) {
        this.recent_meta = response.meta || {};
        return response.objects || response;
    }
});

$(function(){
    var Event = TastypieModel.extend({});

    var Events = TastypieCollection.extend({
        model: Event,
        url: '/api/v1/reservation/'
    }); 
 
    var EventsView = Backbone.View.extend({
        initialize: function(){
            _.bindAll(this, 'addAll', 'render');
            this.collection.bind('reset', this.addAll);
        },
        render: function() {
            this.$el.fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            allDayDefault: false,
            editable: false
            });
        },
        // select: function(start, end, allDay) {
        //     new EventView().render();
        // },
        addAll: function(){
            this.$el.fullCalendar('addEventSource', this.collection.toJSON());
        },
        close: function() {
            this.$el.dialog('close');
        }
    });
 
    var events = new Events();
    new EventsView({el: $("#calendar"), collection: events}).render();
    events.fetch({reset: true});
});