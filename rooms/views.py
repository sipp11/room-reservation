from django.views.generic import TemplateView, FormView, ListView
from django.views.generic.base import ContextMixin
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.template.defaultfilters import slugify
from django.utils.translation import ugettext_lazy as _

from core.views import MessageMixin

from .models import Room

class RoomList(ListView):
    model = Room
    paginate_by = 15

class RoomCreate(MessageMixin, CreateView):
    model = Room
    success_url = reverse_lazy('rooms:list')
    message = _('Successfully Created')


class RoomUpdate(MessageMixin, UpdateView):
    model = Room
    success_url = reverse_lazy('rooms:list')
    message = _('Successfully Updated')


class RoomDelete(MessageMixin, DeleteView):
    model = Room
    success_url = reverse_lazy('rooms:list')
    message = _('Successfully Deleted')
