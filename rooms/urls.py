from django.conf.urls import patterns, url

from .views import RoomList, RoomCreate, RoomDelete, RoomUpdate
from django.contrib.auth.decorators import login_required

urlpatterns = patterns('',
    url(r'^$', RoomList.as_view(), name='list'),

    url(r'^new/$', login_required(RoomCreate.as_view()), name='create'),
    url(r'^update/(?P<slug>[-\w]+)/$', login_required(RoomUpdate.as_view()), name='update'),
    url(r'^delete/(?P<slug>[-\w]+)/$', login_required(RoomDelete.as_view()), name='delete'),
)
