from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.template.defaultfilters import slugify

from django.core.validators import RegexValidator
import re

color_re = re.compile('^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$')
validate_color = RegexValidator(color_re, _(u'Enter a valid color.'), 'invalid')

class Room(models.Model):
    no = models.CharField(_('Room Number'), max_length=20)
    name = models.CharField(_('Room Name'), max_length=50)
    slug = models.SlugField(_("slug"), max_length=60, unique=True)
    color = models.CharField(_("Color"), max_length=10, default="#3071a9", validators=[validate_color])
    capacity = models.PositiveSmallIntegerField(_('Room Capacity'), default=0)
    desc = models.TextField(_('Room Description'), blank=True)

    created_on = models.DateTimeField(_('Created On'), default=timezone.now(), editable=False)
    updated_on = models.DateTimeField(_('Updated On'), editable=False, null=True, blank=True)

    def __unicode__(self):
        return '[%s] %s' % (self.no, self.name)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)[:50]
        self.updated_on = timezone.now()
        super(Room, self).save()