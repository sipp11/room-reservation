from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.authentication import BasicAuthentication, ApiKeyAuthentication, MultiAuthentication, SessionAuthentication
from tastypie.authorization import DjangoAuthorization
from tastypie.exceptions import Unauthorized

from .models import Room


class RoomResource(ModelResource):

    class Meta:
        always_return_data = True
        queryset = Room.objects.all()
        allowed_methods = ['get']
        include_resource_uri = False
