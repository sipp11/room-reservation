from django.contrib import admin
from django.db.models import get_model


class RoomAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}

admin.site.register(get_model('rooms', 'room'), RoomAdmin)