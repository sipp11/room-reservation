from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.authentication import BasicAuthentication, ApiKeyAuthentication, MultiAuthentication, SessionAuthentication
from tastypie.authorization import DjangoAuthorization
from tastypie.exceptions import Unauthorized

from .models import User


class OwnUserAuthorization(DjangoAuthorization):

    def is_authorized(self, request, object=None):
        if request.method == 'GET':
            return True
        elif not request.user.is_staff():
            return False

        return super(OwnUserAuthorization, self).is_authorized(request, object)


class UserResource(ModelResource):

    class Meta:
        always_return_data = True
        queryset = User.objects.all()
        authorization = OwnUserAuthorization()
        authentication = SessionAuthentication()
        list_allowed_methods = ['get']
        detail_allowed_methods = ['get', 'patch']
        excludes = ['password', 'id']
        include_resource_uri = True
