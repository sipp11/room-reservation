from django.conf.urls import patterns, url

from .views import CreateUser, UserProfile, UserList
from django.contrib.auth.decorators import login_required

urlpatterns = patterns('',
    url(r'^private/$', 'users.views.private', name='private'),
    url(r'^register/$', CreateUser.as_view(), name='register'),
    url(r'^login/$', 'django.contrib.auth.views.login',
        {'template_name': 'users/login.html'}, name='login'),
    url(r'^logout/$', 'users.views.logout_user', name='logout'),

    url(r'^profile/(?P<pk>\d+)/$', login_required(UserProfile.as_view()), name='profile'),
    url(r'^users/$', login_required(UserList.as_view()), name='list'),
)
