from django import forms
from .models import Reservation

class ReservationForm(forms.ModelForm):
    class Meta:
        model = Reservation
        exclude = ['approval_note']
        widgets = {
            'created_on': forms.HiddenInput(),
            'approval_status': forms.HiddenInput(),
            'approver': forms.HiddenInput(),
            'approved_on': forms.HiddenInput(),
            'rule': forms.HiddenInput(),
            'calendar': forms.HiddenInput(),
            'creator': forms.HiddenInput(),
            'end_recurring_period': forms.HiddenInput(),
        }


class ApprovingReservationForm(forms.ModelForm):
    class Meta:
        model = Reservation
        exclude = ['title', 'room', 'creator', 'start', 'end', 'description',
                    'created_on', 'rule', 'calendar', 'end_recurring_period']
        widgets = {
            'approver': forms.HiddenInput(),
            'approved_on': forms.HiddenInput(),
        }