from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.authentication import BasicAuthentication, ApiKeyAuthentication, MultiAuthentication, SessionAuthentication
from tastypie.authorization import DjangoAuthorization
from tastypie.exceptions import Unauthorized
import simplejson as json

import time
import datetime

from .models import Reservation
from users.api import UserResource
from rooms.api import RoomResource
from rooms.models import Room

#dthandler = lambda obj: obj.isoformat() if isinstance(obj, datetime.datetime) else No

class OwnAuthentication(SessionAuthentication):

    def is_authenticated(self, request, **kwargs):
        if request.method == 'GET':
            return True
        return super(OwnAuthentication, self).is_authenticated(request, **kwargs)


class OwnAuthorization(DjangoAuthorization):

    def is_authorized(self, request, object=None):
        if request.method == 'GET':
            return True
        return super(OwnAuthorization, self).is_authorized(request, object)


class ReservationResource(ModelResource):
    # status = fields.CharField()
    room = fields.ToOneField(RoomResource, 'room', full=True)
    color = fields.CharField(default="#dd0000")
    approver = fields.ToOneField(UserResource, 'approver', null=True)
    creator = fields.ToOneField(UserResource, 'creator', null=True)

    def hydrate_title(self, bundle):
        # because we prepend 'status' when dehydrating, now get rid of it before saving
        bundle.data['title'] = bundle.obj.title
        return bundle

    def dehydrate_color(self, bundle):
        try: 
            return bundle.obj.room.color
        except:
            return "#ededff"

    def dehydrate_approval_status(self, bundle):
        return bundle.obj.get_approval_status_display()

    def dehydrate_title(self, bundle):
        stat = '/' if bundle.obj.approval_status in ['a'] else '?'
        return '[%s] %s' % (stat, bundle.obj.title)

    class Meta:
        always_return_data = True
        authentication = OwnAuthentication()
        authorization = OwnAuthorization()
        queryset = Reservation.objects.filter(approval_status__in=['p','k','a'])
        list_allowed_methods = ['get']
        detail_allowed_methods = ['get', 'post', 'patch', 'delete']
        excludes = ['end_recurring_period', ]
