from django.views.generic import TemplateView, FormView, ListView, View
from django.views.generic.base import ContextMixin
from django.views.generic.list import MultipleObjectMixin
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.utils.translation import ugettext_lazy as _

from .models import Reservation
from .forms import ReservationForm, ApprovingReservationForm

from core.views import MessageMixin
from braces.views import StaffuserRequiredMixin
import time
from datetime import date

class ReservationReport(StaffuserRequiredMixin, TemplateView):
    model = Reservation
    template_name = 'reservations/report.html'

    def get_context_data(self, **kwargs):
        context = super(ReservationReport, self).get_context_data(**kwargs)
        context['item'] = Reservation.objects.get(pk=kwargs.get('pk'))
        context['duration'] = (context['item'].end - context['item'].start)
        context['today'] = date.today()
        print context['item'].creator.username
        return context


class ReservationHistory(StaffuserRequiredMixin, TemplateView):
    model = Reservation
    template_name = 'reservations/history.html'

    def get_context_data(self, **kwargs):
        context = super(ReservationHistory, self).get_context_data(**kwargs)
        context['object_list'] = Reservation.objects.all().order_by('start')
        return context


class PendingReservationList(StaffuserRequiredMixin, MultipleObjectMixin, TemplateView):
    template_name = 'reservations/pending_list.html'
    paginate_by = 15
    queryset = None
    model = Reservation

    def get_queryset(self):
        self.queryset = Reservation.objects.filter(approval_status__in=['p', 'k'])
        return super(PendingReservationList, self).get_queryset(self)

    def get_context_data(self, **kwargs):
        context = {'object_list': Reservation.objects.filter(approval_status__in=['p', 'k'])}
        context.update(kwargs)
        return super(PendingReservationList, self).get_context_data(**context)


class ReservationList(ListView):
    model = Reservation
    paginate_by = 15

    def get_queryset(self):
        queryset = super(ReservationList, self).get_queryset()
        return queryset.filter(creator=self.request.user)


class ReservationCreate(MessageMixin, CreateView):
    model = Reservation
    form_class = ReservationForm
    success_url = reverse_lazy('reservations:list')
    message = _('Successfully Created')

    def get_form_kwargs(self):
        kwargs = super(ReservationCreate, self).get_form_kwargs()
        kwargs.update({'initial': {'creator': self.request.user } })
        return kwargs


class ReservationUpdate(MessageMixin, UpdateView):
    model = Reservation
    form_class = ReservationForm
    success_url = reverse_lazy('reservations:list')
    message = _('Successfully Updated')


class ReservationDelete(MessageMixin, DeleteView):
    model = Reservation
    success_url = reverse_lazy('reservations:list')
    message = _('Successfully Deleted')
