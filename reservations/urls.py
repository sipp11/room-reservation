from django.conf.urls import patterns, url

from .views import ReservationList, ReservationCreate, ReservationDelete, ReservationUpdate
from .views import PendingReservationList, ReservationHistory, ReservationReport
from django.contrib.auth.decorators import login_required

urlpatterns = patterns('',
    url(r'^$', ReservationList.as_view(), name='list'),
    url(r'^pending/$', PendingReservationList.as_view(), name='pending-list'),
    url(r'^all/$', ReservationHistory.as_view(), name='history'),
    url(r'^report/(?P<pk>\d+)/$', ReservationReport.as_view(), name='report'),
    # url(r'^pending/update/(?P<pk>\d+)/$', ApprovingReservationUpdate.as_view(), name='pending-update'),

    url(r'^new/$', login_required(ReservationCreate.as_view()), name='create'),
    url(r'^update/(?P<pk>\d+)/$', login_required(ReservationUpdate.as_view()), name='update'),
    url(r'^delete/(?P<pk>\d+)/$', login_required(ReservationDelete.as_view()), name='delete'),
)
