from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from schedule.models.events import Event

STATUS_PENDING = 'p'
STATUS_ACKNOWLEDGE = 'k'
STATUS_ACCEPTED = 'a'
STATUS_REJECTED = 'r'

class Reservation(Event):
    STATUS_CHOICES = (
        (STATUS_PENDING, _('Pending')),
        (STATUS_ACKNOWLEDGE, _('Acknowledged')),
        (STATUS_ACCEPTED, _('Accepted')),
        (STATUS_REJECTED, _('Rejected')),
    )
    room = models.ForeignKey('rooms.room', related_name='Room')

    approval_status = models.CharField(_('Approval Status'), max_length=1, choices=STATUS_CHOICES, default=STATUS_PENDING)
    approver = models.ForeignKey('users.user', related_name='Approval Officer', blank=True, null=True)
    approved_on = models.DateTimeField(_('Approved On'), editable=False, null=True, blank=True)
    approval_note = models.CharField(_('Approval Note'), max_length=140, blank=True, 
                                        help_text=_('maximum 140 characters'))

    updated_on = models.DateTimeField(_('Updated On'), editable=False, null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.end < self.start:
            temp = self.start
            self.start = self.end
            self.end = temp

        self.updated_on = timezone.now()
        super(Reservation, self).save()

    def is_pending(self):
        return True if self.approval_status in (STATUS_PENDING, STATUS_ACKNOWLEDGE) else False

    def is_reviewed(self):
        return True if self.approval_status in (STATUS_ACCEPTED, STATUS_REJECTED) else False

    def get_period(self):
        return self.end - self.start